import validatorHelpers from '../../../../utils/validator_helpers.js'

export default function validate(elements, data) {
    const errors = [];
    elements.forEach(element => {
        const name = element.name;
        const value = data[name];
        element.rules.forEach(rule => {
            if (rule in validatorHelpers) {
                const error = validatorHelpers[rule](value);
                if (error) {
                    errors.push({name, error});
                }
            } else {
                throw `No such validation rule: "${rule}"`
            }
        });
    });
    return errors;
}
