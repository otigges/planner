import {LitElement, html} from 'lit-element';

import Choices from "../../../utils/ChoicesWrapper";

class MultiSelect extends LitElement {
    constructor() {
        super();
        this.values = [];
        this._options = [];
    }

    focus(){
        this.select.click();
    }

    static get properties() {
        return {
            values: {type: Array},
            options: {type: Array}
        };
    }

    get value() {
        return [...this.values];
    }

    set options(v){
        const old = this._options;
        const hasChanged = this._options !== v;
        this._options = v;

        if(hasChanged && this.choices){
            this.choices.setChoices(v);
        }

        this.requestUpdate('options', old);
    }

    change() {
        this.values = Array.from(this.shadowRoot.querySelectorAll('option')).filter(option => option.selected).map(option => option.value);
        const event = new CustomEvent('change', {bubbles: true, composed: true});
        this.dispatchEvent(event);
    };

    render() {
        return html`
            <link rel="stylesheet" href="${window.contentsCss}"/>

            <select multiple @change="${this.change}" ?required="${this.hasAttribute('required')}">
                <option value="" disabled>Please select</option>
                ${this._options.map(country => html`<option value="${country.value}">${country.label}</option>`)}
            </select>
        `;
    }

    firstUpdated(changedProperties) {
        super.firstUpdated(changedProperties);

        this.select = this.shadowRoot.querySelector("select");
        this.choices = new Choices(this.select, {
            removeItemButton: true, duplicateItemsAllowed: false
        }, this.shadowRoot);

    }
}

customElements.define('multi-select', MultiSelect);
