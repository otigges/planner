import {LitElement, html, unsafeCSS} from 'lit-element';

const days = [...Array(31)].map((_, i) => (i + 1).toString());
const months = [...Array(12)].map((_, i) => (i + 1).toString());
const currentYear = new Date().getFullYear();
const years = [...Array(100)].map((_, i) => (currentYear - i - 1).toString());

import styles from './styles.cssfrag'

function isLeapYear(year) {
    return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
}

export function getMaxDay(year, month) {
    if (year && month) {
        switch (parseInt(month)) {
            case 2:
                return isLeapYear(parseInt(year)) ? 29 : 28;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            default:
                return 31;
        }
    }
    return 31;
}

class DatePicker extends LitElement {
    constructor() {
        super();

        this.date = {day: "", month: "", year: ""};
        this.maxDay = getMaxDay("", "");
    }

    static get properties() {
        return {
            date: {type: Object},
        };
    }

    static get styles() {
        return unsafeCSS(styles);
    }

    get value() {
        const {year, month, day} = this.date;
        if (day && month && year) {
            return `${year}-${month < 10 ? '0' : ''}${month}-${day < 10 ? '0' : ''}${day}`
        }
        return "";
    }

    focus() {
        if (!this.date.year) {
            this.shadowRoot.querySelector('[name="year"]').focus();
        } else if (!this.date.month) {
            this.shadowRoot.querySelector('[name="month"]').focus();
        } else if (!this.date.day) {
            this.shadowRoot.querySelector('[name="day"]').focus();
        } else {
            this.shadowRoot.querySelector('[name="year"]').focus();
        }
    }

    change(originalEvent) {
        const prop = originalEvent.target.getAttribute('name');
        const value = originalEvent.target.value;

        const newDate = {...this.date, [prop]: value};
        this.maxDay = getMaxDay(newDate.year, newDate.month);
        if (newDate.day > this.maxDay) {
            if (prop === 'day') {
                newDate.month = "";
            } else {
                newDate.day = "";
            }
        }

        this.date = newDate;
            const event = new CustomEvent('change', {bubbles: true});
            this.dispatchEvent(event);
    };

    render() {
        return html`
            <link rel="stylesheet" href="${window.contentsCss}"/>

            <div class="datepicker">
                <select name="year" @change="${this.change}" ?required="${this.hasAttribute('required')}">
                    <option value="" disabled .selected="${this.date.year === ''}">Year</option>
                    ${years.map(year => html`<option value="${year}" .selected="${this.date.year === year}">${year}</option>`)}
                </select>
                <span>-</span>
                <select name="month" @change="${this.change}" ?required="${this.hasAttribute('required')}">
                    <option value="" disabled .selected="${this.date.month === ''}">Month</option>
                    ${months.map(month => html`<option .selected="${this.date.month === month}" value="${month}">${month}</option>`)}
                </select>
                <span>-</span>
                <select name="day" @change="${this.change}" ?required="${this.hasAttribute('required')}">
                    <option value="" disabled .selected="${this.date.day === ''}">Day</option>
                    ${days.map(day => html`<option value="${day}" .selected="${this.date.day === day}">${day}</option>`)}
                </select>
            </div>
        `;
    }

}

customElements.define('date-picker', DatePicker);
