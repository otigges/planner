import "whatwg-fetch";

const GET_FORM_DATA = 'QUERY.GET_FORM_DATA';
const SEND_FORM_DATA = 'QUERY.SEND_FORM_DATA';

class MissingApiResponse extends window.Error {
    constructor() {
        super('MissingApiResponse');
    }
}

class IncompleteApiResponse extends window.Error {
    constructor() {
        super('IncompleteApiResponse');
    }
}

class InvalidApiResponse extends window.Error {
    constructor() {
        super('InvalidApiResponse');
    }
}

async function query(query, options = {}) {
    switch (query) {
        case GET_FORM_DATA: {
            const response = await fetch(`${API_BASEURL}applications/form_specification`)
            if (!response.ok) throw new MissingApiResponse;
            const result = await response.json();
            const {token, formData, templateName} = result;

            if (!formData || !templateName) throw new IncompleteApiResponse;
            return {token, formData, templateName};
        }
        case SEND_FORM_DATA: {
            const {formData} = options;
            const response = await fetch(
                `${API_BASEURL}applications/create/`,
                {
                    method: 'POST',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify(formData),
                }
            )
            if (!response.ok) throw new MissingApiResponse();
            const result = await response.json();

            if (!result.ok) throw new InvalidApiResponse();
            return {};
        }
        default:
            throw new Error(`Unknown query "${query}"`);
    }
}

export {
    GET_FORM_DATA,
    SEND_FORM_DATA,
    query
}
