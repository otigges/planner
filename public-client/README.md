# Public Client

This contains the public client (i.e. javascript) code. Public here means
available for non-seawatch folks. Currently, it is only the application form.

## Setting up the project

1. Ensure you have a recent nodejs version installed (unsure which version we
   actually need, `node --version` prints `v14.9.0` for me)
2. `npm install` in the public-client folder

## Building/running the project for development

Build the project using `npm run build`. If you're developing, using `npm run
watch` open will probably speed up your feedback loop. It automatically rebuilds
on file changes.

The code is served to the browser by the `public backend` because the js code
needs the public backend url, which is injected dynamically. The public backend
always uses the latest compiled code, so with `npm run watch`, you can edit
a file and reload the browser page.

(TODO: Does hot module reloading work for us? If not, can we make it work?)

## Building for production
The public client code is included in the public backend docker image.
Currently, we are using the same webpack config as we use for development, which
probably isn't ideal.

## Tests

We have some unit tests in the `test` folder. They can be run using `npm run
test`. This is a bit slow because it does a webpack build for each test file,
but it can be significantly sped up during development by using `npm run
test:watch`, which will use the webpack watch mode.

## Dev Notes

- Originally, the public client started out with the idea of getting a form
    definition from the backend and then rendering whatever fields were defined
    there. We simplified the concept since then, but if something seems
    overengineered, it may very well be left over from that time.
- The custom elements (a.k.a web components) are located at
    `src/js/app/elements`
- We are using web components with `lit-element` and `lit-html`.
    [open-wc](https://open-wc.org/) has been a great resource for that.
- Unittests mostly follow the open-wc ideas (karma with puppeteer to run the
    unittests in a real browser), but while open-wc suggests not building
    anything at all for tests and only using a bundler for production, we use
    `karma-webpack`. This is for two reasons:
    1. Using npm dependencies that aren't provided as es6 modules. It is
       possible to use [snowpack](https://www.snowpack.dev/) to convert them,
       which we did in the past.
    2. PostCSS. We need to run postcss on all css for auto-prefixing and such,
       which requires that we have them in separate files and not inline them in
       the webcomponents as suggested by open-wc.
    We considered moving from webpack to rollup and using [rollup-plugin-postcss-lit](https://github.com/umbopepato/rollup-plugin-postcss-lit), but we cut our losses there and went with karma-webpack.
    Efforts to improve this situation are appreciated, but please be aware that
    we already spent a significant amount of time and energy on this.

