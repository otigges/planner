import "@babel/polyfill/noConflict";

import {fixture, expect, nextFrame} from "@open-wc/testing";
import "../../src/js/app/elements/FormHandler/index.js";
import "../../src/js/app/elements/MultiSelect/index.js";
import "../../src/js/app/elements/ValidationFeedback/index.js";
import {elementUpdated} from "@open-wc/testing-helpers";

describe("FormHandler", () => {
    beforeEach(() => {
        window.history.pushState(null, null, "") // otherwise history.state would remain "success" after form submission
    })

    it('should show success page after form submit', async () => {
        const formHandler = await fixture("<form-handler></form-handler>");

        formHandler.sendFormData = async () => ({});

        await completeForm(formHandler);
        formHandler.submit();
        await elementUpdated(formHandler);

        expect(formHandler.shadowRoot.querySelector("h1")).dom.to.equal("<h1>Thanks a lot for your interest!</h1>")
    });

    it('can show validation messages on different fields when submission fails', async () => {
        const formHandler = await fixture("<form-handler></form-handler>");
        const firstNameFeedback = formHandler.shadowRoot.querySelector(".field_first_name validation-feedback");
        const lastNameFeedback = formHandler.shadowRoot.querySelector(".field_last_name validation-feedback");
        const message = 'This field is required';

        // empty form is attempted to be submitted
        formHandler.submit();
        await elementUpdated(formHandler);
        expect(firstNameFeedback.getAttribute('message')).to.equal(message);
        expect(firstNameFeedback.shadowRoot.querySelector('.feedback')).dom.to.equal(`<div class="feedback">${message}</div>`);

        expect(lastNameFeedback.getAttribute('message')).to.be.null;
        expect(lastNameFeedback.shadowRoot.querySelector('.feedback')).dom.to.equal(`<div class="feedback"></div>`);

        // now the first_name is provided
        formHandler.shadowRoot.querySelector('[name="first_name"]').value = 'me';
        formHandler.shadowRoot.querySelector('[name="first_name"]').dispatchEvent(new Event('change', {bubbles: true}));
        formHandler.submit();
        await elementUpdated(formHandler);

        expect(firstNameFeedback.getAttribute('message')).to.be.null;
        expect(firstNameFeedback.shadowRoot.querySelector('.feedback')).dom.to.equal(`<div class="feedback"></div>`);

        expect(lastNameFeedback.getAttribute('message')).to.equal(message);
        expect(lastNameFeedback.shadowRoot.querySelector('.feedback')).dom.to.equal(`<div class="feedback">${message}</div>`);
    });

    it("Should re-render form after submitting and click on back button", async () => {
        // given
        let formHandler = await fixture("<form-handler></form-handler>");
        formHandler.sendFormData = () => Promise.resolve()

        // when we submit our form data
        await completeForm(formHandler);
        formHandler.submit();
        await elementUpdated(formHandler);
        expect(window.history.state).to.equal('success');
        expect(formHandler.shadowRoot.querySelector("h1")).dom.to.equal("<h1>Thanks a lot for your interest!</h1>")

        // and we click on the back button
        let ev = new window.PopStateEvent("popstate", {state: null})
        window.dispatchEvent(ev)
        await elementUpdated(formHandler);

        // then we expect a re-rendered form
        expect(formHandler.shadowRoot.querySelector("h1")).dom.to.equal("<h1>Hi, we are excited to meet you!</h1>")
    })
});

async function completeForm(formHandler) {
    const type = function (name, value) {
        const element = formHandler.shadowRoot.querySelector(`[name="${name}"]`);
        element.value = value;
        element.dispatchEvent(new Event('change', {bubbles: true}));
    };
    const typeInComponent = function (name, value, innerSelector) {
        const element = formHandler.shadowRoot.querySelector(`[name="${name}"]`);
        const input = element.shadowRoot.querySelector(innerSelector);
        input.value = value;
        input.dispatchEvent(new Event('input', {bubbles: true}));
    };
    const pickFirstEntry = async function (name) {
        const element = formHandler.shadowRoot.querySelector(`[name="${name}"]`);
        element.focus();
        await nextFrame();
        element.shadowRoot.querySelector('input').dispatchEvent(createEnterEvent());
        element.dispatchEvent(new Event('change', {bubbles: true}));
    };
    const setDate = function (name, year, month, day) {
        const element = formHandler.shadowRoot.querySelector(`date-picker[name="${name}"]`);
        element.shadowRoot.querySelector(`[name="year"]`).value = year;
        element.shadowRoot.querySelector('[name="year"]').dispatchEvent(new Event('change', {bubbles: true}));
        element.shadowRoot.querySelector(`[name="month"]`).value = month;
        element.shadowRoot.querySelector('[name="month"]').dispatchEvent(new Event('change', {bubbles: true}));
        element.shadowRoot.querySelector(`[name="day"]`).value = day;
        element.shadowRoot.querySelector('[name="day"]').dispatchEvent(new Event('change', {bubbles: true}));
    };
    type('first_name', 'Doro');
    type('last_name', 'Thee');
    type('phone_number', '+123');
    type('email', 'ab@cd.com');
    setDate('date_of_birth', '2000', '12', '31');
    type('gender', 'none_other');
    await pickFirstEntry('nationalities');
    await pickFirstEntry('spoken_languages');
    typeInComponent('motivation', 'high', 'textarea');
    typeInComponent('qualification', 'granted', 'textarea');
    await nextFrame();
}

function createEnterEvent() {
    const event = document.createEvent('Events');
    event.initEvent('keydown', true, true);
    event.keyCode = event.which = event.charCode = 13;
    event.key = event.code = 'Enter';
    return event;
}
