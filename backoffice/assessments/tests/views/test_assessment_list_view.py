from django.test import Client, TestCase
from django.urls import reverse

from assessments.models import Assessment
from assessments.tests import base
from seawatch_registration.models import Position
from seawatch_registration.tests.factories import (
    PositionFactory,
    ProfileFactory,
    UserFactory,
)

from ..factories import AssessmentFactory


class TestAssessmentListView(TestCase):
    def setUp(self):
        self.client = Client()
        self.url = reverse("assessment_list")

    def test_requires_authentication(self):
        base.assert_redirects_to_login(reverse("assessment_list"), self.client)

    def test_requires_permission(self):
        base.assert_requires_permission(
            reverse("assessment_list"), "can_assess_profiles", self.client
        )

    def test_views__show_assessments__get__should_show_text_when_no_pending_assessments_are_avaiable(
        self,
    ):
        # Arrange
        user = UserFactory(permission="can_assess_profiles")
        self.client.force_login(user)

        # Act
        response = self.client.get(self.url, user=user)

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "assessment-list.html")
        self.assertContains(response, "There are no pending assessments!")

    def test_views__show_assessments__get__should_show_text_when__assessment_is_status_rejected(
        self,
    ):
        # Arrange
        profile = ProfileFactory(requested_positions=[Position.objects.first()])
        AssessmentFactory(profile=profile, status=Assessment.REJECTED)

        user = UserFactory(permission="can_assess_profiles")

        # Act
        self.client.force_login(user)
        response = self.client.get(self.url, user=user)

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "assessment-list.html")
        self.assertContains(response, "There are no pending assessments!")

    def test_views__show_assessments__get__should_show_text_when__assessment_is_status_approved(
        self,
    ):
        # Arrange
        AssessmentFactory(status=Assessment.ACCEPTED)
        user = UserFactory(permission="can_assess_profiles")

        # Act
        self.client.force_login(user)
        response = self.client.get(self.url, user=user)

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "assessment-list.html")
        self.assertContains(response, "There are no pending assessments!")

    def test_views__show_assessments__get__should_show_table_when_pending_assessment_is_available(
        self,
    ):
        # Arrange
        id = 917943
        first_name = "winnie"
        last_name = "the pooh"
        position = "most esteemed invoator"
        AssessmentFactory(
            id=id,
            status=Assessment.PENDING,
            profile=ProfileFactory(
                user=UserFactory(first_name=first_name, last_name=last_name),
                requested_positions=[PositionFactory(name=position)],
            ),
        )

        crewing_user = UserFactory(permission="can_assess_profiles")

        # Act
        self.client.force_login(crewing_user)
        response = self.client.get(self.url, user=crewing_user)

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "assessment-list.html")
        self.assertContains(response, self.td(first_name + " " + last_name))
        self.assertContains(response, self.td("pending"))
        self.assertContains(response, position)
        self.assertContains(response, self.tr_onclick(id))

    @staticmethod
    def td(value):
        return "<td>" + str(value) + "</td>"

    @staticmethod
    def tr_onclick(primary_key):
        return (
            "<tr onclick=\"window.location='"
            + reverse("assessment_update", kwargs={"pk": primary_key})
            + "';\">"
        )
