from inspect import isclass

from django.core.exceptions import PermissionDenied
from django.test.client import RequestFactory
from django.views import View
from pytest import raises

from .factories import UserFactory


def assert_requires_profile(rf: RequestFactory, view):
    """Asserts that the view requires the logged-in user to have a profile

        The view argument can be either a subclass of django.views.View or a classic
        django view function.
    """
    request = rf.get("ignored_url")
    request.user = UserFactory(profile=None)

    if isclass(view) and issubclass(view, View):
        view = view.as_view()

    with raises(PermissionDenied):
        view(request)
