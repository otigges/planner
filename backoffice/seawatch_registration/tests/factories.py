from datetime import timedelta
from typing import Sequence as SequenceType
from typing import Type

from django.contrib.auth.models import Permission, User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.views import View
from factory import (
    DjangoModelFactory,
    Faker,
    LazyAttribute,
    LazyFunction,
    SubFactory,
    post_generation,
)

from seawatch_registration import countries

from ..models import (
    Answer,
    Availability,
    Document,
    DocumentType,
    Position,
    Profile,
    Question,
    Skill,
)


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ("email",)

    first_name = Faker("first_name")
    last_name = Faker("last_name")
    email = Faker("email")
    username = Faker("user_name")
    is_active = True
    is_superuser = False

    @post_generation
    def permission(obj: User, create: bool, extracted: str, **kwargs):
        if create and extracted is not None:
            obj.user_permissions.set([get_permission(extracted)])

    @post_generation
    def permissions(obj: User, create: bool, extracted: SequenceType[str], **kwargs):
        if create and extracted is not None:
            obj.user_permissions.set(map(get_permission, extracted))

    @post_generation
    def view_permissions(obj: User, create: bool, view: Type[View], **kwargs):
        """This is meant to work with django.contrib.auth.mixins.PermissionRequiredMixin
        """
        if create and view is not None:
            permissions = getattr(view, "permission_required", [])
            if isinstance(permissions, str):
                permissions = [permissions]
            obj.user_permissions.set(map(get_permission, permissions))


def get_permission(permission_str: str) -> Permission:
    if "." not in permission_str:
        return Permission.objects.get(codename=permission_str)
    else:
        app_label, codename = permission_str.split(".")
        return Permission.objects.get(
            content_type__app_label=app_label, codename=codename
        )


class PositionFactory(DjangoModelFactory):
    class Meta:
        model = Position

    name = Faker("job")


class ProfileFactory(DjangoModelFactory):
    class Meta:
        model = Profile
        django_get_or_create = ("user",)

    user = SubFactory(UserFactory)
    date_of_birth = Faker("date_of_birth")
    needs_schengen_visa = Faker("boolean")
    citizenship = LazyFunction(
        lambda: Faker(
            "random_sample",
            elements=[d["code"] for d in countries.data],
            length=Faker("pyint", min_value=0, max_value=8).generate(),
        ).generate()
    )

    @post_generation
    def requested_positions(obj, create, extracted, **kwargs):
        if create and extracted is not None:
            obj.requested_positions.set(extracted)

    @post_generation
    def approved_positions(obj, create, extracted, **kwargs):
        if create and extracted is not None:
            obj.approved_positions.set(extracted)

    @post_generation
    def skills(obj, create, extracted, **kwargs):
        if create and extracted is not None:
            obj.skills.set(extracted)


class SkillFactory(DjangoModelFactory):
    class Meta:
        model = Skill

    name = Faker("random_element")
    description = Faker("sentence")
    group = Faker(
        "random_element",
        elements=[group_identifier for (group_identifier, _) in Skill.SKILL_GROUPS],
    )


class DocumentTypeFactory(DjangoModelFactory):
    class Meta:
        model = DocumentType

    name = Faker("word")
    group = Faker(
        "random_element",
        elements=[
            group_identifier
            for (group_identifier, _) in DocumentType.DOCUMENT_TYPE_GROUPS
        ],
    )


class QuestionFactory(DjangoModelFactory):
    class Meta:
        model = Question

    text = Faker("question")
    mandatory = Faker("boolean")


class AnswerFactory(DjangoModelFactory):
    class Meta:
        model = Answer

    text = Faker("sentence")
    question = SubFactory(QuestionFactory)
    profile = SubFactory(ProfileFactory)


class DocumentFactory(DjangoModelFactory):
    class Meta:
        model = Document

    document_type = SubFactory(DocumentTypeFactory)
    profile = SubFactory(ProfileFactory)
    number = Faker("random_number")
    issuing_date = Faker("past_date", start_date="-10y")
    expiry_date = Faker("date_between", start_date="-2y", end_date="+5y")
    issuing_authority = Faker("word")
    issuing_place = Faker("city")
    issuing_country = Faker("country")
    file = LazyFunction(
        lambda: SimpleUploadedFile(
            f"{Faker('word').generate()}.jpg",
            Faker("binary", length=812).generate(),
            content_type="image/jpg",
        )
    )


class AvailabilityFactory(DjangoModelFactory):
    class Meta:
        model = Availability

    profile = SubFactory(ProfileFactory)
    start_date = Faker("future_date", end_date="+3y")

    end_date = LazyAttribute(
        lambda availability: Faker(
            "date_between",
            start_date=availability.start_date,
            end_date=(
                availability.start_date
                + timedelta(days=Faker("pyint", min_value=1, max_value=300))
            ),
        ).generate()
    )

    comment = Faker("sentence")
