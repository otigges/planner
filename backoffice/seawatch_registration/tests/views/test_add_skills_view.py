from django.test import Client, RequestFactory
from django.urls import reverse
from pytest import mark
from pytest_django.asserts import assertContains, assertRedirects, assertTemplateUsed

from seawatch_registration.tests.asserts import assert_requires_profile
from seawatch_registration.tests.factories import (
    ProfileFactory,
    SkillFactory,
    UserFactory,
)

from ...views.skill import SkillsUpdateView


@mark.django_db
def test_skill_update_requires_profile(rf: RequestFactory):
    assert_requires_profile(rf, SkillsUpdateView)


url = reverse("skill_update")


@mark.django_db
def test_skill_update_should_render_with_form_html_when_profile_exists(client: Client):
    # Arrange
    user = UserFactory()
    ProfileFactory(user=user)
    client.force_login(user)

    # Act
    response = client.get(url)

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "form.html")


@mark.django_db
def test_skill_update_should_show_selected_skills_when_skills_exists(client: Client):
    # Arrange
    user = UserFactory()
    ProfileFactory(
        user=user, skills=[SkillFactory(group="other"), SkillFactory(group="lang")]
    )
    client.force_login(user)

    # Act
    response = client.get(url)

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "form.html")
    assertContains(response, 'checked="" class="form-check-input" id="id_languages_')
    assertContains(response, 'checked="" class="form-check-input" id="id_skills_')


@mark.django_db
def test_skill_update_should_show_selected_skills_when_skills_exists(client: Client):
    # Arrange
    user = UserFactory()
    ProfileFactory(
        user=user, skills=[SkillFactory(group="other"), SkillFactory(group="lang")]
    )
    client.force_login(user)

    # Act
    response = client.get(url)

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "form.html")
    assertContains(response, 'checked="" class="form-check-input" id="id_languages_')
    assertContains(response, 'checked="" class="form-check-input" id="id_skills_')


@mark.django_db
def test_skill_update_should_redirect_to_document_when_skills_are_set_to_zero(
    client: Client,
):
    # Arrange
    user = UserFactory()
    language = SkillFactory(group="lang")
    profile = ProfileFactory(user=user, skills=[language, SkillFactory(group="other")])
    client.force_login(user)

    # Act
    response = client.post(url + "?initial_registration", {"languages": language.id})

    # Assert
    assert len(profile.skills.all()) == 1
    assertRedirects(
        response, expected_url=f"{reverse('document_create')}?initial_registration=yes"
    )


@mark.django_db
def test_skill_update_should_redirect_to_document_when_skills_are_set_to_2(
    client: Client,
):
    # Arrange
    skill = SkillFactory(group="other")
    language = SkillFactory(group="lang")

    user = UserFactory()
    profile = ProfileFactory(user=user, skills=[language, skill])
    client.force_login(user)

    # Act
    response = client.post(
        url + "?initial_registration", {"skills": skill.id, "languages": language.id},
    )

    # Assert
    assertRedirects(
        response, expected_url=f"{reverse('document_create')}?initial_registration=yes"
    )
    assert len(profile.skills.all()) == 2
