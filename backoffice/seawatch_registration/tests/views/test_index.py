from bs4 import BeautifulSoup
from django.shortcuts import reverse
from pytest import mark

from seawatch_registration.tests.factories import UserFactory

from ...models import Position
from ...views.index import IndexView
from ..factories import PositionFactory


def request_index_html(rf):
    request = rf.get("ignored")
    request.user = UserFactory()
    response = IndexView.as_view()(request)
    return BeautifulSoup(response.rendered_content, features="html.parser")


@mark.django_db
def test_link_to_applications(rf):
    html = request_index_html(rf)

    assert html.find("a", href=reverse("application_list"))


@mark.django_db
def test_links_to_applications_by_position(rf):
    # For some reason, there's a set of Positions in the database. The assert is here to
    # make sure we remove this when it becomes unneccessary
    assert Position.objects.count() != 0
    Position.objects.all().delete()

    PositionFactory(name="simply the best")

    html = request_index_html(rf)

    assert html.find(
        "a", href=f"{reverse('application_list')}?position=simply%20the%20best"
    )
    assert (
        len(
            [
                a
                for a in html.find_all("a")
                if a["href"].startswith(f"{reverse('application_list')}?")
            ]
        )
        == 1
    )
