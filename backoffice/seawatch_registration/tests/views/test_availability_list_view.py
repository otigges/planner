from django.test import Client, RequestFactory
from django.urls import reverse
from pytest import mark
from pytest_django.asserts import assertContains, assertTemplateUsed

from ...views.availability import AvailabilityListView
from ..asserts import assert_requires_profile
from ..factories import AvailabilityFactory, ProfileFactory, UserFactory


@mark.django_db
def test_requires_profile(rf: RequestFactory):
    assert_requires_profile(rf, AvailabilityListView)


url = reverse("availability_list")


@mark.django_db
def test_renders_with_availability_list_html_when_profile_exists(client: Client):
    # Arrange
    user = UserFactory()
    ProfileFactory(user=user)

    # Act
    client.force_login(user)
    response = client.get(url)

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "./seawatch_registration/availability_list.html")


@mark.django_db
def test_shows_existing_availabilities(client: Client):
    # Arrange
    user = UserFactory()
    profile = ProfileFactory(user=user)
    AvailabilityFactory(
        profile=profile,
        start_date="2002-02-02",
        end_date="2010-10-10",
        comment="comment1",
    )
    AvailabilityFactory(
        profile=profile,
        start_date="2012-12-12",
        end_date="2013-01-01",
        comment="comment2",
    )

    # Act
    client.force_login(user)
    response = client.get(url)

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "./seawatch_registration/availability_list.html")
    assertContains(response, 'name="availability_set-TOTAL_FORMS" value="2"')
    assertContains(response, 'value="2002-02-02"')
    assertContains(response, 'value="2010-10-10"')
    assertContains(response, 'value="comment1"')
    assertContains(response, 'value="2012-12-12"')
    assertContains(response, 'value="2013-01-01"')
    assertContains(response, 'value="comment2"')
