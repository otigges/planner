from django.test import Client, RequestFactory
from django.urls import reverse
from pytest import mark
from pytest_django.asserts import assertNotContains, assertRedirects, assertTemplateUsed

from seawatch_registration.models import Availability

from ...views.availability import CreateView
from ..asserts import assert_requires_profile
from ..factories import ProfileFactory, UserFactory


@mark.django_db
def test_requires_profile(rf: RequestFactory):
    assert_requires_profile(rf, CreateView)


url = reverse("availability_create")


@mark.django_db
def test_renders_with_availability_form_html(client: Client):
    # Arrange
    user = UserFactory()
    ProfileFactory(user=user)
    client.force_login(user)

    # Act
    response = client.get(url)

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "seawatch_registration/availability_form.html")


@mark.django_db
def test_adds_availability(client: Client):
    # Arrange
    user = UserFactory()
    profile = ProfileFactory(user=user)
    client.force_login(user)

    # Act
    response = client.post(
        url, {"profile": profile, "start_date": "2010-10-10", "end_date": "2012-12-12"},
    )

    # Assert
    assertRedirects(response, reverse("availability_list"))
    assert Availability.objects.count() == 1
    assert Availability.objects.first().start_date.strftime("%Y-%m-%d") == "2010-10-10"
    assert Availability.objects.first().end_date.strftime("%Y-%m-%d") == "2012-12-12"


@mark.django_db
def test_renders_error_when_form_is_invalid(client: Client):
    # Arrange
    user = UserFactory()
    profile = ProfileFactory(user=user)
    client.force_login(user)

    # Act
    response = client.post(
        url, {"profile": profile, "start_date": "2012-12-12", "end_date": "2010-10-10"},
    )

    # Assert
    assertTemplateUsed(response, "seawatch_registration/availability_form.html")
    assert Availability.objects.count() == 0
    assert response.status_code == 200
    assertNotContains(response, "alert-success")
