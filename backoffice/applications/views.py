from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core import mail
from django.db import transaction
from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404, reverse
from django.urls import reverse_lazy
from django.views.decorators.http import require_POST
from django.views.generic import DeleteView, DetailView, ListView, TemplateView

from .models import Application, ImportedApplication, Volunteer
from .service import acknowledge_import, import_application, request_availabilities


class ApplicationListView(PermissionRequiredMixin, ListView):
    model = Application

    permission_required = "applications.view_application"

    def get_queryset(self):
        qs = super().get_queryset()

        position = self.request.GET.get("position")
        if position:
            qs = qs.filter(requested_positions__name=position)

        return qs


class ApplicationDetailView(PermissionRequiredMixin, DetailView):
    model = Application

    permission_required = "applications.view_application"


class ImportApplicationView(PermissionRequiredMixin, TemplateView):
    template_name = "applications/import_application.html"

    permission_required = (
        "applications.view_application",
        "applications.add_application",
    )

    # This can be overridden by passing a kwarg to as_view(). The function passed in
    # must accept 0 arguments and return an ImportedApplicationDTO or None.
    # (I don't know why we need self here.)
    import_application = lambda self: import_application()  # noqa
    acknowledge_import = lambda self, application_uid: acknowledge_import(  # noqa
        application_uid
    )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["imported_application"] = ImportedApplication.objects.order_by(
            "created"
        ).first()
        return context

    def get(self, request):
        if not ImportedApplication.objects.exists():
            self.__import_application_from_public_backend()
        return super().get(request)

    def __import_application_from_public_backend(self):
        application_data = self.import_application()
        if application_data is not None:
            uid = application_data.uid
            application = ImportedApplication.from_data_transfer_object(
                application_data
            )
            application.save()

            for spoken in application_data.spoken_languages:
                application.languages.create(code=spoken.code, points=spoken.points)

            self.acknowledge_import(uid)

    def post(self, request, *args, **kwargs):
        imported_application_id = request.POST["application_id"]
        with transaction.atomic():
            imported_application = get_object_or_404(
                ImportedApplication, pk=imported_application_id
            )
            application = Application.from_imported_application(imported_application)
            application.save()

            for spoken in imported_application.languages.all():
                application.languages.create(code=spoken.code, points=spoken.points)

            imported_application.delete()
        messages.success(request, f"{application.name}'s application has been imported")
        return HttpResponseRedirect(reverse("application_import"))


class ApplicationDeleteView(PermissionRequiredMixin, DeleteView):
    model = Application
    success_url = reverse_lazy("application_list")
    permission_required = "applications.delete_application"


@require_POST
@permission_required("applications.delete_application", raise_exception=True)
@permission_required("applications.add_volunteer", raise_exception=True)
def accept_application(request, pk):
    application = get_object_or_404(Application, pk=pk)
    Volunteer.objects.create(
        name=application.name, phone_number=application.phone_number
    )
    application.delete()

    return HttpResponseRedirect(reverse("application_list"))


class VolunteerListView(PermissionRequiredMixin, ListView):
    model = Volunteer

    permission_required = "applications.view_volunteer"

    # overwrite by passing function as kwargs
    request_availabilities = lambda self, email: request_availabilities(email)  # noqa

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs):
        recepient = request.POST["request_availability"]
        recepient_user = Volunteer.objects.filter(name=recepient).first()
        recepient_email = recepient_user.email
        self.request_availabilities(recepient_email)
        return HttpResponseRedirect(reverse("volunteer_list"))
