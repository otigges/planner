import atexit
from shutil import rmtree
from tempfile import mkdtemp

from .base import *

DEBUG = True

SECRET_KEY = "insecure"

DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": ":memory:"}}

MEDIA_ROOT = mkdtemp()
atexit.register(lambda: rmtree(MEDIA_ROOT))
