# Public Backend

## Setting up the project

 - Ensure you have python installed in a version >= 3.8.5 :

 `python3 --version`
 - To work locally it is recommended to configure a python virtual environment. To achieve this do the following steps:
```
python3 -m venv .env
source .env/bin/activate
pip install --upgrade pip
pip install -r requirements.txt -r requirements-dev.txt
```
## Running the project for development

## Configure the app to use dev settings
- If you're using direnv, it will happen automatically
- If not, you can create an empty file ".use-dev-settings" in the repo-root (the
    one that contains the folders "backoffice", "public-backend" and
    "public-client")

## Start the server
 `./manage.py runserver`
