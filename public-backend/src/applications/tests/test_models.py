import pytest
from django.core.exceptions import ValidationError
from phonenumber_field.phonenumber import PhoneNumber, to_python

from ..models import Application
from .factories import ApplicationFactory


def test_application_defaults_to_random_uuid():
    assert len(Application().uid) > 30
    assert Application().uid != Application().uid


@pytest.mark.django_db
def test_application_phone_number_requires_international_prefix():
    assert (
        to_python(
            ApplicationFactory(phone_number="asd").data["phone_number"]
        ).is_valid()
        == False
    )
    assert (
        to_python(
            ApplicationFactory(phone_number="00492837482993").data["phone_number"]
        ).is_valid()
        == False
    )
    assert to_python(
        ApplicationFactory(phone_number="+492837482993").data["phone_number"]
    ).is_valid()
