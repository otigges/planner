from django.contrib import admin
from django.urls import include, path

from . import views

urlpatterns = [
    path("create/", views.application_create, name="application_create"),
    path("form_specification/", views.form_specification, name="form_specification"),
    path("oldest", views.application_detail_oldest, name="application_detail_oldest"),
    path("<uuid:uid>", views.application_delete, name="application_delete"),
    path(
        "request_availabilities",
        views.application_request_availabilities,
        name="application_request_availabilities",
    ),
]
