from django.conf import settings
from django.http import HttpResponseForbidden


def requires_token_authentication(view):
    def wrapped(request, *args, **kwargs):
        try:
            token_header = request.META["HTTP_AUTHORIZATION"]
        except KeyError:
            return HttpResponseForbidden()

        if token_header == f"Token {settings.PLANNER_PUBLIC_BACKEND_AUTH_TOKEN}":
            return view(request, *args, **kwargs)
        else:
            return HttpResponseForbidden()

    return wrapped
