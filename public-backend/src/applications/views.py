import json
import os
from http import HTTPStatus

import phonenumbers
from django.conf import settings
from django.core import mail
from django.core.exceptions import ValidationError as DjangoValidationError
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods, require_POST
from django.views.generic import TemplateView, View
from jsonschema import validate
from jsonschema.exceptions import ValidationError as SchemaValidationError

from .decorators import requires_token_authentication
from .models import Application, Language

with open(
    os.path.join(settings.COMMON_DIR, "schema", "initialApplication.json")
) as json_file:
    schema = json.load(json_file)


@require_POST
@csrf_exempt
def application_create(request):
    data = json.loads(request.body)

    try:
        validate(instance=data, schema=schema)
    except SchemaValidationError as e:
        return JsonResponse(status=422, data={"ok": False, "message": e.args[0]})

    try:
        Application.objects.create(data=data,)

    except KeyError as e:
        return JsonResponse(
            status=422, data={"ok": False, "message": f"Missing key: {e.args[0]}"}
        )
    except DjangoValidationError as e:
        return JsonResponse(status=422, data={"ok": False, "message": e.args[0]})

    return JsonResponse({"ok": True})


def form_specification(request):
    return JsonResponse(
        {
            "ok": True,
            "templateName": "initial",
            "formData": {"first_name": "", "phone_number": ""},
        }
    )


@requires_token_authentication
def application_detail_oldest(request):
    try:
        application = Application.objects.order_by("created")[0:1].get()
    except Application.DoesNotExist:
        return HttpResponse(status=HTTPStatus.NO_CONTENT)
    else:
        application = {
            "uid": application.uid,
            "created": application.created,
            "data": application.data,
        }
        return JsonResponse(application)


@csrf_exempt
@requires_token_authentication
@require_http_methods(["DELETE"])
def application_delete(request, uid):
    try:
        application = Application.objects.get(uid=uid)
        application.delete()
    except Application.DoesNotExist:
        return HttpResponse(status=HTTPStatus.NOT_FOUND)
    else:
        return JsonResponse({"ok": True})


class HomeView(TemplateView):
    template_name = "applications/index.html"


@requires_token_authentication
def application_request_availabilities(request):
    email = request.GET["email"]
    send_message_count = mail.send_mail(
        subject="Mission availability request",
        message="We need to know if you are available for upcoming missions",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[email],
    )
    ok = True if send_message_count == 1 else False
    return JsonResponse({"ok": ok})
