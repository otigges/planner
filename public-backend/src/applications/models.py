from datetime import date, datetime, timezone
from uuid import uuid4

import jsonfield
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField


def uuid4_str():
    return str(uuid4())


class Gender(models.TextChoices):
    MALE = "male", _("Male")
    FEMALE = "female", _("Female")
    OTHER = "none_other", _("None/Other")
    PREFER_NOT_SAY = "prefer-not-say", _("Prefer not to say")


class Application(models.Model):
    uid = models.CharField(max_length=255, default=uuid4_str)
    created = models.DateTimeField(editable=False)
    data = jsonfield.JSONField()

    def save(self, *args, **kwargs):
        """ On save, create timestamp """
        if not self.id and self.created is None:
            self.created = datetime.now(timezone.utc)

        if f"{self.data['date_of_birth']}" > f"{date.today()}":
            raise ValidationError("Date of birth has to be in the past.")

        return super().save(*args, **kwargs)


class Language(models.Model):
    code = models.CharField(max_length=3)
    points = models.IntegerField()

    applicant = models.ForeignKey(Application, on_delete=models.CASCADE)
