const puppeteer = require('puppeteer');
const jestscreenshot = require('@jeeyah/jestscreenshot');
const path = require('path');
const fetch = require('node-fetch');

async function submitForm(browser, name) {
    const page = await browser.newPage();

    await page.goto(PUBLIC_FRONTEND_URL);
    await page.waitForSelector('form-handler');

    const formHandler = (await page.waitForFunction(() => document.querySelector('form-handler').shadowRoot)).asElement();

    const firstNameInput = await formHandler.$(`input[name='first_name']`);
    await firstNameInput.type(name);

    const lastNameInput = await formHandler.$('input[name="last_name"]')
    await lastNameInput.type("lastname_" + name)


    const dateOfBirthElement = (
        await formHandler.evaluateHandle(node => node.querySelector('date-picker[name="date_of_birth"]').shadowRoot)
    ).asElement();
    await (await dateOfBirthElement.$('[name="year"]')).select('2000');
    await (await dateOfBirthElement.$('[name="month"]')).select('1');
    await (await dateOfBirthElement.$('[name="day"]')).select('1');

    const phoneNumberInput = await formHandler.$('input[name="phone_number"]')
    await phoneNumberInput.type("+491634884421")

    const emailInput = await formHandler.$('input[name="email"]')
    await emailInput.type(name + "@example.com");

    const genderInput = await formHandler.$('[name="gender"]');
    await genderInput.select('none_other');

    const languageElem = (
      await formHandler.evaluateHandle(
        node => node.querySelector('[name=spoken_languages]').shadowRoot
      )
    ).asElement();
    const languageInput = await languageElem.$(".choices");
    await languageInput.click();

    const choicesList = await languageInput.asElement().$(".choices__list div")
    await choicesList.click()

    const motivationElement = (
        await formHandler.evaluateHandle(
            node => node.querySelector('[name="motivation"]').shadowRoot
        )
    ).asElement();
    const motivationInput = await motivationElement.$('textarea')
    await motivationInput.type("motivated!!")

    const qualificationElement = (
        await formHandler.evaluateHandle(
            node => node.querySelector('[name="qualification"]').shadowRoot
        )
    ).asElement();
    const qualificationInput = await qualificationElement.$('textarea')
    await qualificationInput.type("qualificated!!")

    // await page.waitForFunction(name => document.querySelector("form-handler").shadowRoot.querySelector("input[name='first_name']").value = name, {}, name);
    await page.waitForFunction(() => document.querySelector("form-handler").shadowRoot.querySelector("input[name='first_name']").dispatchEvent(new CustomEvent("change", {bubbles: true})));

    await page.waitForFunction(() => document.querySelector("form-handler").shadowRoot.querySelector("select[name='gender'] option[value='prefer_not_say']").selected = true);
    await page.waitForFunction(() => document.querySelector("form-handler").shadowRoot.querySelector("select[name='gender']").dispatchEvent(new CustomEvent("change", {bubbles: true})));

    const nationalitiesElement = (
        await formHandler.evaluateHandle(
            node => node.querySelector('[name="nationalities"]').shadowRoot
        )
    ).asElement();
    const nationalitiesInput = await nationalitiesElement.$('input');
    await nationalitiesInput.type("tajik");
    await nationalitiesInput.press('Enter');

    const spokenLanguagesElement = (
        await formHandler.evaluateHandle(
            node => node.querySelector('[name="spoken_languages"]').shadowRoot
        )
    ).asElement();
    const spokenLanguagesInput = await spokenLanguagesElement.$('input');
    await spokenLanguagesInput.type("tajik");
    await spokenLanguagesInput.press('Enter');

    await Promise.all([
        page.waitForFunction(() => !document.querySelector("form-handler").shadowRoot.querySelector("[type='submit']").click()),
        page.waitForFunction(() => document.querySelector("form-handler").shadowRoot.innerHTML.includes("Thanks"))
    ]);
    await page.close();
}

function submit(page) {
    return Promise.all([
        page.waitForNavigation(),
        page.click('input[type="submit"], button[type="submit"]')
    ]);
}

describe('Initial application', () => {
    let browser = null;
    let page = null;

    beforeAll(async () => {
        browser = await puppeteer.launch(LAUNCH_OPTIONS);
        page = await browser.newPage();

        await jestscreenshot.init({
            page: page,
            dirName: __dirname,
            scriptName: path.basename(__filename).replace('.js', ''),
        });
    });

    it('supports our journey', async () => {
        await page.goto(BACKOFFICE_URL);
        await page.type('#id_username', BACKOFFICE_ADMIN_USERNAME);
        await page.type('#id_password', BACKOFFICE_ADMIN_PASSWORD);
        await submit(page);

        await page.waitForFunction(
            () => document.querySelector(".container").innerText.includes("Willkommen zur")
        );

        await page.goto(BACKOFFICE_URL + '/applications/import');
        var html = await page.$eval('p', e => e.innerText);
        expect(html).toBe('No new applications');

        await submitForm(browser, 'Anna');
        await submitForm(browser, 'Peter');

        await page.goto(BACKOFFICE_URL + '/applications/import');
        html = await page.$eval('p', e => e.innerHTML);
        expect(html).toContain('Name: Anna');

        await page.goto(BACKOFFICE_URL + '/applications/import');
        await submit(page);
        await page.waitForFunction(() => document.querySelector(".container").innerText.includes("Name:"));
        html = await page.$eval('p', e => e.innerText);
        expect(html).toBe('Name: Peter');

        await page.goto(BACKOFFICE_URL + '/applications/import');
        // await page.waitForFunction('document.querySelector(".container").innerText.includes("Name:")');
        html = await page.$eval('p', e => e.innerText);
        expect(html).toBe('Name: Peter');
        await submit(page);

        await page.goto(BACKOFFICE_URL + '/applications/import');
        html = await page.$eval('p', e => e.innerText);
        expect(html).toBe('No new applications');
    }, TIMEOUT);

    it('can redirect users to the seawatch position descriptions', async () => {
        const page = await browser.newPage();
        const pageTarget = page.target();

        await page.goto(PUBLIC_FRONTEND_URL);
        await page.waitForSelector('form-handler');

        const formHandler = (await page.waitForFunction(() => document.querySelector('form-handler').shadowRoot)).asElement();

        const positionsElement = (
            await formHandler.evaluateHandle(
                node => node.querySelector('[name="positions"]').shadowRoot
            )
        ).asElement();
        const positionsInput = await positionsElement.$('input');
        await positionsInput.type("Cook");
        await positionsInput.press('Enter');
        await positionsInput.type("Chief Engineer");
        await positionsInput.press('Enter');
    
        const buttons = (
            await formHandler.evaluateHandle(
                node => node.querySelector('[name="qualification"]').shadowRoot.querySelector('.buttonlist')
            )
        ).asElement();
    
        const cookButton = (await buttons.evaluateHandle(node => node.querySelectorAll('a')[0])).asElement();
        const chiefButton = (await buttons.evaluateHandle(node => node.querySelectorAll('a')[1])).asElement();
        const noThirdButton = (await buttons.evaluateHandle(node => node.querySelectorAll('a')[2])).asElement();
    
        expect(await page.evaluate(element => element.textContent, cookButton)).toEqual('Cook');
        expect(await page.evaluate(element => element.textContent, chiefButton)).toEqual('Chief Engineer');
        expect(noThirdButton).toBeNull();
    
        await cookButton.click();
    
        const newTarget = await browser.waitForTarget(
            target => target.opener() === pageTarget,
            { timeout: 3000}
        );
        const newPage = await newTarget.page();
        await newPage.waitForNavigation({ waitUntil: 'load' });

        const title = await newPage.title();
        const url = await newPage.evaluate('location.href');
        expect(title).toEqual('Join the Crew • Sea-Watch e.V.');
        expect(url).toEqual('https://sea-watch.org/en/join-us/crew/#1479866201423-eed45755-ec79');

        await newPage.close();
        await page.close();
     }, TIMEOUT);

    afterAll(() => {
        jestscreenshot.cleanup(function () {
            if (browser) {
                browser.close();
            }
        });
    });
});

describe('send email', () => {
    let browser = null;
    let page = null;

    beforeAll(async () => {
        browser = await puppeteer.launch(LAUNCH_OPTIONS);
        page = await browser.newPage();

        await jestscreenshot.init({
            page: page,
            dirName: __dirname,
            scriptName: path.basename(__filename).replace('.js', ''),
        });
    });
    it('sends an email when i request availabilities', async () => {
        await page.goto(BACKOFFICE_URL);
        await page.type('#id_username', BACKOFFICE_ADMIN_USERNAME);
        await page.type('#id_password', BACKOFFICE_ADMIN_PASSWORD);
        await submit(page);
        await page.waitForFunction(
            () => document.querySelector(".container").innerText.includes("Willkommen zur")
        );
        await page.goto(BACKOFFICE_URL + '/applications/volunteers');
        await page.waitForFunction(
            () => document.querySelector(".container").innerText.includes("test")
        );
        await page.click('input[type="submit"], button[type="submit"]');
        const webmailResponse = await fetch( WEBMAIL_URL + "/mail" );
        const emails = await webmailResponse.json();
        expect(emails).toHaveProperty("mailItems");
        const mailItems = emails.mailItems;
        expect(mailItems).toBeInstanceOf(Array);
        const email = mailItems[0];
        expect(email.fromAddress).toBe("backoffice@test.com")
        expect(email.toAddresses).toContain("test@example.com");
        expect(email.subject).toBe("Mission availability request");
        expect(email.body).toContain("We need to know if you are available for upcoming missions");
    }, TIMEOUT);
    afterAll(() => {
        jestscreenshot.cleanup(function () {
            if (browser) {
                browser.close();
            }
        });
    });
});
